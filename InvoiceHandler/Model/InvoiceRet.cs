﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    public class InvoiceRet
    {
        // Optional, repeatable IDTYPE (string or custom type parsing string)
        [XmlElement(ElementName = "TxnID")]
        public List<string> TxnIDs { get; set; }

        // Required
        [XmlElement(ElementName = "TimeCreated")]
        public DateTime TimeCreated { get; set; }

        // Required
        [XmlElement(ElementName = "TimeModified")]
        public DateTime TimeModified { get; set; }

        // Required
        [XmlElement(ElementName = "EditSequence")]
        public string EditSequence { get; set; }

        // Required
        [XmlElement(ElementName = "TxnNumber")]
        public int TxnNumber { get; set; }

        //Optional
        [XmlElement(ElementName = "CustomerRef")]
        public CustomerRef CustomerRef { get; set; }

        //Optional
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }

        [XmlElement(ElementName = "InvoiceLineRet")]
        public List<InvoiceLineRet> InvoiceLineRets { get; set; }

        // Optional
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }
}
