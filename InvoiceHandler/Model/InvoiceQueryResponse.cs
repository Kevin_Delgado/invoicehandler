﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    [XmlRoot(ElementName = "InvoiceQueryRs")]
    public class InvoiceQueryResponse
    {
        #region Attributes

        [XmlAttribute]
        public int statusCode { get; set; }
        [XmlAttribute]
        public string statusSeverity { get; set; }
        [XmlAttribute]
        public string statusMessage { get; set; }
        [XmlAttribute]
        public int retCount { get; set; }
        [XmlAttribute]
        public int iteratorRemainingCount { get; set; }
        [XmlAttribute]
        public Guid iteratorID { get; set; }
        [XmlAttribute]
        public int requestID { get; set; }

        #endregion

        #region Elements

        [XmlElement(ElementName = "InvoiceRet")]
        public List<InvoiceRet> Invoices { get; set; }

        #endregion
    }
}
