﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <notes>
    /// Another one with the same ListID/Fullname structure
    /// </notes>
    public class OverrideUOMSetRef
    {
        // Optional (string or custom type parsed from string?)
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        // Optional
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }
}
