﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    public class InvoiceLineGroupRet
    {
        // Optional (string or custom type parsed from string?)
        [XmlElement(ElementName = "TxnLineID")]
        public string TxnLineID { get; set; }

        // Required
        [XmlElement(ElementName = "ItemGroupRef")]
        public ItemGroupRef ItemGroupRef { get; set; }

        [XmlElement(ElementName = "Desc")]
        public string Desc { get; set; }

        // Optional (QUANTYPE? Is Quickbooks aliasing different numeric types)
        [XmlElement(ElementName = "Quantity")]
        public int Quantity { get; set; }

        // Optional
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }

        // Optional
        [XmlElement(ElementName = "OverrideUOMSetRef")]
        public OverrideUOMSetRef OverrideUOMSetRef { get; set; }

        [XmlElement(ElementName = "IsPrintItemsInGroup")]
        public bool IsPrintItemsInGroup { get; set; }

        [XmlElement(ElementName = "TotalAmount")]
        public Decimal TotalAmount { get; set; }

        // Required
        [XmlElement(ElementName = "InvoiceLineRet")]
        public List<InvoiceLineRet> InvoiceLineRets { get; set; }

        // Optional
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }
}
