﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <notes>
    /// In the 'schema' just before this is <!-- BEGIN OR -->
    /// Ask what that means in this context.
    /// </notes>
    public class InvoiceLineRet
    {
        // Optional (string or custom type parsed from string?)
        [XmlElement(ElementName = "TxnLineID")]
        public string TxnLineID { get; set; }

        // Required
        [XmlElement(ElementName = "ItemRef")]
        public ItemRef ItemRef { get; set; }

        [XmlElement(ElementName = "Desc")]
        public string Desc { get; set; }

        // Optional (QUANTYPE? Is Quickbooks aliasing different numeric types)
        [XmlElement(ElementName = "Quantity")]
        public int Quantity { get; set; }

        // Optional
        [XmlElement(ElementName = "UnitOfMeasure")]
        public string UnitOfMeasure { get; set; }

        // Optional
        [XmlElement(ElementName = "OverrideUOMSetRef")]
        public OverrideUOMSetRef OverrideUOMSetRef { get; set; }

        //Optional
        [XmlElement(ElementName = "Rate")]
        public Decimal Rate { get; set; }

        //Optional
        [XmlElement(ElementName = "RatePercent")]
        public Decimal RatePercent { get; set; }

        //Optional
        [XmlElement(ElementName = "ClassRef")]
        public ClassRef ClassRef { get; set; }

        [XmlElement(ElementName = "Amount")]
        public Decimal Amount { get; set; }

        // Optional
        [XmlElement(ElementName = "Other1")]
        public string Other1 { get; set; }

        // Optional
        [XmlElement(ElementName = "Other2")]
        public string Other2 { get; set; }

        // Optional
        [XmlElement(ElementName = "DataExtRet")]
        public DataExtRet DataExtRet { get; set; }
    }
}
