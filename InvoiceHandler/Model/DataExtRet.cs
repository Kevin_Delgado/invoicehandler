﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    public class DataExtRet
    {
        // Optional, repeatable IDTYPE (string or custom type parsing string)
        [XmlElement(ElementName = "OwnerID")]
        public List<Guid> OwnerIDs { get; set; }

        // Optional
        [XmlElement(ElementName = "DataExtName")]
        public string DataExtName { get; set; }

        // Required
        /// <summary>
        /// 
        /// </summary>
        /// <notes>
        /// Possible types here are:
        /// AMTTYPE, DATETIMETYPE, INTTYPE, PERCENTTYPE, PRICETYPE, QUANTYPE, STR1024TYPE, STR255TYPE
        /// I need to find a sample to figure out how to handle properly
        /// Using Object for now
        /// </notes>
        [XmlElement(ElementName = "DataExtType")]
        public Object DataExtType { get; set; }

        // Required
        [XmlElement(ElementName = "DataExtValue")]
        public string DataExtValue { get; set; }
    }
}
