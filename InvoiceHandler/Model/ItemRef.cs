﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace InvoiceHandler.Model
{
    /// <summary>
    /// 
    /// </summary>
    /// <notes>
    /// CustomerRef, ClassRef and this all have the same structure of
    /// ListID & FullName, but one has ListID as required. May be
    /// able to condense. Some other ones as well.
    /// </notes>
    public class ItemRef
    {
        // Optional (string or custom type parsed from string?)
        [XmlElement(ElementName = "ListID")]
        public string ListID { get; set; }

        // Optional
        [XmlElement(ElementName = "FullName")]
        public string FullName { get; set; }
    }
}
