﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Xml.Linq;
using InvoiceHandler.Model;
using System.Diagnostics;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace InvoiceHandler
{
    /// <summary>
    /// Interaction logic for InvoiceHandlerWindow.xaml
    /// </summary>
    public partial class InvoiceHandlerWindow : Window
    {
        public InvoiceHandlerWindow()
        {
            InitializeComponent();
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
            if (openFileDialog.ShowDialog() == true)
                txtEditor.Text = File.ReadAllText(openFileDialog.FileName);
        }

        private void btnParseInvoice_Click(object sender, RoutedEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtEditor.Text))
            {
                try
                {
                    XDocument xDoc = XDocument.Parse(txtEditor.Text);

                    XmlSerializer serializer = new XmlSerializer(typeof(InvoiceQueryResponse));

                    // Should validate the incoming xml before working with it

                    using (XmlReader reader = xDoc.CreateReader())
                    {
                        InvoiceQueryResponse iqr = serializer.Deserialize(reader) as InvoiceQueryResponse;

                        txtParsed.Text = JsonConvert.SerializeObject(iqr, Newtonsoft.Json.Formatting.Indented);
                    }
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }
    }
}
