## Choose an xml file to process

1. Build the app.
2. Run the app.
3. Click the **Open File** button.
4. Browse to find and open an XML file to process (three are included)
5. The contents of the XML will be displayed in the upper text box, click **Parse Invoice** to convert the xml data into C# object(s).
6. The values/structure of the objects created will be displayed in JSON format in the lower text box.